﻿using Moq;
using NUnit.Framework;
using Roslyn.Compilers;
using Roslyn.Compilers.CSharp;
using Roslyn.Services;
using System;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;

namespace RefactorPredicates.Test
{
    [TestFixture]
    public class RefactorExpressionTest
    {
        IDocument document;
        DocumentId documentId;
        string sourceText { get; set; }

        void Setup()
        {
            ProjectId projectId;
            var solution = Solution.Create(SolutionId.CreateNewId())
                .AddProject("MyProject", "MyProject", LanguageNames.CSharp, out projectId)
                .AddMetadataReference(projectId, MetadataReference.CreateAssemblyReference("mscorlib"))
                .AddMetadataReference(projectId, MetadataReference.CreateAssemblyReference("Microsoft.CSharp"))
                .AddMetadataReference(projectId, MetadataReference.CreateAssemblyReference("System"))
                .AddMetadataReference(projectId, MetadataReference.CreateAssemblyReference("System.Core"))
                .AddMetadataReference(projectId, MetadataReference.CreateAssemblyReference("System.Web"))
                .AddDocument(projectId, @"C:\Users\Brian\Dropbox\ExpressionDestroyer\RefactorPredicates\RefactorPredicates.Test\TestCompilationUnits\Test.cs", sourceText, out documentId);

            document = solution.GetDocument(documentId);
            
            var sourceNode = SyntaxTree.ParseText(sourceText).GetRoot();
            document = document.UpdateSyntaxRoot(sourceNode);
        }

        [Test]
        public void RefactorsSimilarButNotEqualLambdas()
        {
            sourceText = @"
            using System;
            using System.Collections.Generic;
            using System.Linq;

            class Program
            {
                static void enumerate()
                {
                    var lst = new int[1];
                    var testInt = 5;
                    var testInt2 = 6;
                    var test1 = lst.Where(x => x == testInt || true);
                    var test2 = lst.Where(x => x == testInt2 || true);
                }
            }
            ";

            Setup();

            var refactoringProvider = new CodeRefactoringProvider();
            var editor = refactoringProvider.GetRefactoring(document, new TextSpan(374, 25), CancellationToken.None);

            var editedDocument = editor.Actions.First().GetEdit();

            var edt = editedDocument.UpdatedSolution.GetDocument(documentId).GetText(CancellationToken.None).ToString();

            Assert.AreEqual(@"using System;
            using System.Collections.Generic;
            using System.Linq;

            class Program
            {
                static void enumerate()
                {
                    var lst = new int[1];
                    var testInt = 5;
                    var testInt2 = 6;
                    var test1 = lst.Where(IntEqualstestIntOrTrue(testInt));
                    var test2 = lst.Where(IntEqualstestIntOrTrue(testInt2));
                }

                private static Func<int, bool> IntEqualstestIntOrTrue(int testInt)
                {
                    return x => x == testInt || true;
                }
            }".CollapseCode(), edt.CollapseCode());
        }


        [Test]
        public void BringsInSystemAndDoesUseSystemFuncQualifiedName()
        {
            sourceText = @"
            using System.Collections.Generic;
            using System.Linq;

            class Program
            {
                static void Main()
                {
                    var lst = new int[1];
                    var sy = string.Empty;
                    var test1 = lst.Where(x => x.ToString() == sy);
                    var test2 = lst.Where(x => x.ToString() == sy);
                }
            }
            ";

            Setup();

            var refactoringProvider = new CodeRefactoringProvider();
            var editor = refactoringProvider.GetRefactoring(document, new TextSpan(309, 23), CancellationToken.None);

            var edt = editor.Actions.First().GetEdit().UpdatedSolution.GetDocument(documentId).GetText().ToString();

            Assert.IsFalse(edt.Contains("using System;"));
            Assert.IsTrue(edt.Contains("System.Func"));
        }

        [Test]
        public void ModifiesClassNestedInNamespace()
        {
            sourceText = @"
            using System.Collections.Generic;
            using System.Linq;
            using System;

            namespace TestNameSpace 
            {
                class Program
                {
                    static void Main()
                    {
                        var lst = new int[1];
                        var sy = string.Empty;
                        var test1 = lst.Where(x => x.ToString() == sy);
                        var test2 = lst.Where(x => x.ToString() == sy);
                    }
                }
            }
            ";

            Setup();

            var refactoringProvider = new CodeRefactoringProvider();
            var editor = refactoringProvider.GetRefactoring(document, new TextSpan(417, 23), CancellationToken.None);

            var edt = editor.Actions.First().GetEdit().UpdatedSolution.GetDocument(documentId).GetText().ToString();

            Assert.AreEqual(@"using System.Collections.Generic;
            using System.Linq;
            using System;

            namespace TestNameSpace
            {
                class Program
                {
                    static void Main()
                    {
                        var lst = new int[1];
                        var sy = string.Empty;
                        var test1 = lst.Where(XToStringEqualssy(sy));
                        var test2 = lst.Where(XToStringEqualssy(sy));
                    }

                    private static Func<int, bool> XToStringEqualssy(string sy)
                    {
                        return x => x.ToString() == sy;
                    }
                }
            }".CollapseCode(), edt.CollapseCode());
            Assert.IsFalse(edt.Contains("System.Func"));
        }

        [Test]
        public void AbleToDealWithStaticFunctionCallAndDoesntPassObjForStaticCall()
        {
            sourceText = @"
            using System;
            using System.Linq;

            class Program
            {
                static void Main()
                {
                    var lst = new int[1];
                    lst.ToList().ForEach(x => Console.WriteLine(x));
                    lst.ToList().ForEach(x => Console.WriteLine(x));
                }
            }
            ";

            Setup();

            var refactoringProvider = new CodeRefactoringProvider();
            var editor = refactoringProvider.GetRefactoring(document, new TextSpan(244, 25), CancellationToken.None);

            var editedDocument = editor.Actions.First().GetEdit();

            var edt = editedDocument.UpdatedSolution.GetDocument(documentId).GetText(CancellationToken.None).ToString();
            Assert.IsFalse(edt.Contains("(Console)"));
            Assert.IsTrue(edt.Contains("Action<"));
        }

        [Test]
        public void KeepsEquivalentExpressionsSeparateBasedOnType()
        {
            sourceText = @"
            using System;
            using System.Linq;

            class Program
            {
                static void Main()
                {
                    var lst = new int[1];
                    var lst2 = new string[1];
                    var test3 = lst.Where(x => x.ToString() == string.Empty);
                    var test4 = lst.Where(x => x.ToString() == string.Empty);
                    var test5 = lst2.Where(x => x.ToString() == string.Empty);
                    var test6 = lst2.Where(x => x.ToString() == string.Empty);
                }
            }
            ";

            Setup();

            var refactoringProvider = new CodeRefactoringProvider();
            var editor = refactoringProvider.GetRefactoring(document, new TextSpan(292, 33), CancellationToken.None);

            var editedDocument = editor.Actions.First().GetEdit();

            var edt = editedDocument.UpdatedSolution.GetDocument(documentId).GetText(CancellationToken.None).ToString();
            
            Assert.AreEqual(@"using System;
            using System.Linq;

            class Program
            {
                static void Main()
                {
                    var lst = new int[1];
                    var lst2 = new string[1];
                    var test3 = lst.Where(XToStringEqualsEmpty());
                    var test4 = lst.Where(XToStringEqualsEmpty());
                    var test5 = lst2.Where(x => x.ToString() == string.Empty);
                    var test6 = lst2.Where(x => x.ToString() == string.Empty);
                }

                private static Func<int, bool> XToStringEqualsEmpty()
                {
                    return x => x.ToString() == string.Empty;
                }
            }".CollapseCode(), edt.CollapseCode());
        }

        [Test]
        [ExpectedException(ExpectedException= typeof(NullReferenceException))]
        public void CallContainsThisAndWontBeDetected()
        {
            sourceText = @"
            using System;
            using System.Linq;

            class Program
            {
                void Main()
                {
                    var lst = new int[1];
                    var test3 = lst.Where(x => x == this.getInt());
                    var test4 = lst.Where(x => x == this.getInt());
                }
                int getInt()
                {
                    return 0;
                }
            }
            ";

            Setup();

            var refactoringProvider = new CodeRefactoringProvider();
            var editor = refactoringProvider.GetRefactoring(document, new TextSpan(238, 18), CancellationToken.None);

            var editedDocument = editor.Actions.First().GetEdit();
        }

        [Test]
        public void RefactorLambdaMethodWithMultipleParametersInNewFunctionSignature()
        {
            sourceText = @"
            using System;
            using System.Linq;

            class Program
            {
                void Main()
                {
                    var lst = new int[1];
                    int k = 0;
                    int j = 1;
                    var test3 = lst.Where(x => x == k || x == j);
                    var test4 = lst.Where(x => x == k || x == j);
                }
            }
            ";

            Setup();

            var refactoringProvider = new CodeRefactoringProvider();
            var editor = refactoringProvider.GetRefactoring(document, new TextSpan(302, 21), CancellationToken.None);

            var editedDocument = editor.Actions.First().GetEdit();

            var edt = editedDocument.UpdatedSolution.GetDocument(documentId).GetText(CancellationToken.None).ToString();

            Assert.AreEqual(@"using System;
            using System.Linq;

            class Program
            {
                void Main()
                {
                    var lst = new int[1];
                    int k = 0;
                    int j = 1;
                    var test3 = lst.Where(IntEqualskOrintEqualsj(k, j));
                    var test4 = lst.Where(IntEqualskOrintEqualsj(k, j));
                }

                private static Func<int, bool> IntEqualskOrintEqualsj(int k, int j)
                {
                    return x => x == k || x == j;
                }
            }".CollapseCode(), edt.CollapseCode());
        }

        [Test]
        public void RefactorLambdaWithMemberAccessOnObjectInsideLambda()
        {
            sourceText = @"
            using System;
            using System.Linq;

            class Program
            {
                void Main()
                {
                    var lst = new int[1];
                    var tup = new Tuple<int,int>(1,0);
                    var test3 = lst.Where(x => x == tup.Item1 || x == tup.Item2);
                    var test4 = lst.Where(x => x == tup.Item1 || x == tup.Item2);
                }
            }
            ";

            Setup();

            var refactoringProvider = new CodeRefactoringProvider();
            var editor = refactoringProvider.GetRefactoring(document, new TextSpan(294, 37), CancellationToken.None);

            var editedDocument = editor.Actions.First().GetEdit();

            var edt = editedDocument.UpdatedSolution.GetDocument(documentId).GetText(CancellationToken.None).ToString();

            Assert.AreEqual(@"using System;
            using System.Linq;

            class Program
            {
                void Main()
                {
                    var lst = new int[1];
                    var tup = new Tuple<int, int>(1, 0);
                    var test3 = lst.Where(IntEqualstupItem1OrintEqualstupItem2(tup));
                    var test4 = lst.Where(IntEqualstupItem1OrintEqualstupItem2(tup));
                }

                private static Func<int, bool> IntEqualstupItem1OrintEqualstupItem2(Tuple<int, int> tup)
                {
                    return x => x == tup.Item1 || x == tup.Item2;
                }
            }".CollapseCode(), edt.CollapseCode());
        }

        [Test]
        public void RefactorLambdasOnlyInSameClass()
        {
            sourceText = @"
            using System;
            using System.Linq;

            class Program
            {
                void Main()
                {
                    var lst = new int[1];
                    var tup = new Tuple<int, int>(1, 0);
                    var test3 = lst.Where(x => x == tup.Item1 || x == tup.Item2);
                    var test4 = lst.Where(x => x == tup.Item1 || x == tup.Item2);
                }
            }

            class Program2
            {
                void Main2()
                {
                    var lst = new int[1];
                    var tup = new Tuple<int, int>(1, 0);
                    var test3 = lst.Where(x => x == tup.Item1 || x == tup.Item2);
                }
            }
            ";

            Setup();

            var refactoringProvider = new CodeRefactoringProvider();
            var editor = refactoringProvider.GetRefactoring(document, new TextSpan(296, 37), CancellationToken.None);

            var editedDocument = editor.Actions.First().GetEdit();

            var edt = editedDocument.UpdatedSolution.GetDocument(documentId).GetText(CancellationToken.None).ToString();

            Assert.AreEqual(@"using System;
            using System.Linq;

            class Program
            {
                void Main()
                {
                    var lst = new int[1];
                    var tup = new Tuple<int, int>(1, 0);
                    var test3 = lst.Where(IntEqualstupItem1OrintEqualstupItem2(tup));
                    var test4 = lst.Where(IntEqualstupItem1OrintEqualstupItem2(tup));
                }

                private static Func<int, bool> IntEqualstupItem1OrintEqualstupItem2(Tuple<int, int> tup)
                {
                    return x => x == tup.Item1 || x == tup.Item2;
                }
            }

            class Program2
            {
                void Main2()
                {
                    var lst = new int[1];
                    var tup = new Tuple<int, int>(1, 0);
                    var test3 = lst.Where(x => x == tup.Item1 || x == tup.Item2);
                }
            }".CollapseCode(), edt.CollapseCode());
        }

        [Test]
        public void RefactorLambdasNotThrownOffBySecondClass()
        {
            sourceText = @"
            using System;
            using System.Linq;

            class Program
            {
                void Main()
                {
                    var lst = new int[1];
                    var tup = new Tuple<int, int>(1, 0);
                    var test3 = lst.Where(x => x == tup.Item1 || x == tup.Item2);
                }
            }

            class Program2
            {
                void Main2()
                {
                    var lst = new int[1];
                    var tup = new Tuple<int, int>(1, 0);
                    var test3 = lst.Where(x => x == tup.Item1 || x == tup.Item2);
                    var test4 = lst.Where(x => x == tup.Item1 || x == tup.Item2);
                }
            }
            ";

            Setup();

            var refactoringProvider = new CodeRefactoringProvider();
            var editor = refactoringProvider.GetRefactoring(document, new TextSpan(296, 37), CancellationToken.None);

            var editedDocument = editor.Actions.First().GetEdit();

            var edt = editedDocument.UpdatedSolution.GetDocument(documentId).GetText(CancellationToken.None).ToString();

            Assert.AreEqual(@"using System;
            using System.Linq;

            class Program
            {
                void Main()
                {
                    var lst = new int[1];
                    var tup = new Tuple<int, int>(1, 0);
                    var test3 = lst.Where(IntEqualstupItem1OrintEqualstupItem2(tup));
                    var test4 = lst.Where(IntEqualstupItem1OrintEqualstupItem2(tup));
                }

                private static Func<int, bool> IntEqualstupItem1OrintEqualstupItem2(Tuple<int, int> tup)
                {
                    return x => x == tup.Item1 || x == tup.Item2;
                }
            }

            class Program2
            {
                void Main2()
                {
                    var lst = new int[1];
                    var tup = new Tuple<int, int>(1, 0);
                    var test3 = lst.Where(x => x == tup.Item1 || x == tup.Item2);
                }
            }".CollapseCode(), edt.CollapseCode());
        }
    }
}
