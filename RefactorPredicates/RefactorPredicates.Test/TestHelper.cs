﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefactorPredicates.Test
{
    public static class TestHelper
    {
        public static string CollapseCode(this string code)
        {
            return code.Replace("\n", "").Replace("\r", "").Replace(" ","");
        }
    }
}
