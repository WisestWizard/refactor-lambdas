﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Roslyn.Compilers.Common;
using Roslyn.Compilers.CSharp;

namespace RefactorPredicates
{
    public static class CastCommon
    {
        public static IEnumerable<SimpleLambdaExpressionSyntax> CastTokens(this IEnumerable<CommonSyntaxNodeOrToken> tokens)
        {
            var nodeList = new List<SimpleLambdaExpressionSyntax>();
            foreach (var v in tokens)
            {
                nodeList.Add((SimpleLambdaExpressionSyntax)v);
            }

            return nodeList;
        }

        public static IEnumerable<SimpleLambdaExpressionSyntax> CastTokens2(this IEnumerable<SyntaxNodeOrToken> tokens)
        {
            var nodeList = new List<SimpleLambdaExpressionSyntax>();
            foreach (var v in tokens)
            {
                nodeList.Add((SimpleLambdaExpressionSyntax)v);
            }

            return nodeList;
        }
    }
}
