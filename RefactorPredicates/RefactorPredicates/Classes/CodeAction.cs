﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Roslyn.Compilers;
using Roslyn.Compilers.Common;
using Roslyn.Compilers.CSharp;
using Roslyn.Services;
using System.Text;

namespace RefactorPredicates
{
    public class CodeAction : ICodeAction
    {
        private IDocument document;
        private IList<SimpleLambdaExpressionSyntax> semanticNodes;
        private SyntaxAnnotation semanticNodeAnnotation;
        private CompilationUnitSyntax compilationModel;

        public CodeAction(IDocument document, IList<SimpleLambdaExpressionSyntax> node)
        {
            semanticNodeAnnotation = new SyntaxAnnotation();
            compilationModel = (CompilationUnitSyntax)document.GetSyntaxRoot();

            this.document = document.UpdateSyntaxRoot(compilationModel.ReplaceNodes(node, (s, d) => s.WithAdditionalAnnotations(semanticNodeAnnotation)));
            this.semanticNodes = this.document.GetSyntaxRoot().GetAnnotatedNodesAndTokens(semanticNodeAnnotation).CastTokens().ToList();
        }

        public string Description
        {
            get { return "Refactor duplicate Lambda Expressions in class"; }
        }

        public CodeActionEdit GetEdit(CancellationToken cancellationToken)
        {            
            var semanticModel = (SemanticModel)document.GetSemanticModel(cancellationToken);
            compilationModel = (CompilationUnitSyntax)document.GetSyntaxRoot();

            //Check that we're using System;
            var isUsingSystem = compilationModel.ChildNodes().OfType<UsingDirectiveSyntax>().Any(us => us.ToString() == "using System;");

            //if (!isUsingSystem)
            //{
            //    compilationModel = compilationModel.AddUsings(Syntax.UsingDirective(Syntax.ParseName("System")));
            //    document = document.UpdateSyntaxRoot(compilationModel);
            //    compilationModel = (CompilationUnitSyntax)document.GetSyntaxRoot();

            //    //Recompute semantic model first so you can find the span in the node later in compilation model
            //    semanticModel = (SemanticModel)document.GetSemanticModel();
            //    semanticNodes = semanticModel.SyntaxTree.GetRoot().GetAnnotatedNodesAndTokens(semanticNodeAnnotation).CastTokens2().ToList();
            //}

            var classDeclaration = GetClassDeclaration();

            var lambdasToReplace = compilationModel.GetAnnotatedNodesAndTokens(semanticNodeAnnotation).CastTokens2();

            var name = PredicateLocator.LambdaName(semanticNodes.First(), semanticModel, classDeclaration);

            //Call Site
            compilationModel = compilationModel
            .ReplaceNodes(lambdasToReplace,
            (s, t) =>
                Syntax.InvocationExpression(Syntax.IdentifierName(name))
                .WithArgumentList(Syntax.ArgumentList(
                GetArgumentList(PredicateLocator.ExtractFunctionParameters(semanticModel, s)))));

            //Muted, so reget class declaration -- consider adding annotation
            classDeclaration = GetClassDeclaration();

            // Construct a new Method -- Use the lambda function with the most different parameters
            IEnumerable<IdentifierNameAndType> paramsAndTypes = new List<IdentifierNameAndType>();
            for (int i = 0; i < semanticNodes.Count; i++)
            {
                var funcHeaderParams = PredicateLocator.ExtractFunctionParameters(semanticModel, semanticNodes.ElementAt(i));
                if (funcHeaderParams.Count() > paramsAndTypes.Count())
                {
                    paramsAndTypes = funcHeaderParams;
                }
            }

            var existingLambdaExpression = classDeclaration.ChildNodes().OfType<MemberDeclarationSyntax>();

            var newMethodDeclaration =
                DeclareMethodWithType(name, semanticModel)
                        .WithModifiers(
                           GetDeclaration(semanticModel))
                        .WithParameterList(Syntax.ParameterList(GetParameterList(paramsAndTypes)))
                        .WithBody(
                            Syntax.Block(
                                Syntax.List<StatementSyntax>(
                                    Syntax.ReturnStatement(Syntax.ParseExpression(semanticNodes.First().ToFullString())
                                       ))));

            // Add this new MethodDeclarationSyntax to the above ClassDeclarationSyntax.
            var newClassDeclaration =
                classDeclaration.AddMembers(newMethodDeclaration);

            // Update the CompilationUnitSyntax with the new ClassDeclarationSyntax.
            compilationModel = compilationModel.ReplaceNode(classDeclaration, newClassDeclaration);

            return new CodeActionEdit(document.UpdateSyntaxRoot(compilationModel.NormalizeWhitespace()));
        }

        private ClassDeclarationSyntax GetClassDeclaration()
        {
            ClassDeclarationSyntax classDeclaration;
            var namespaceDeclaration = compilationModel.ChildNodes()
                .OfType<NamespaceDeclarationSyntax>().FirstOrDefault();

            //If class is inside namespace
            if (namespaceDeclaration != null)
            {
                classDeclaration = GetClassDeclarationFromDocument(namespaceDeclaration);
            }
            else
            {
                //If they went rogue and only have a class
                classDeclaration = GetClassDeclarationFromDocument(compilationModel);
            }
            return classDeclaration;
        }

        private SyntaxTokenList GetDeclaration(SemanticModel model)
        {
            //Should always be static.
            return Syntax.TokenList(
                                            Syntax.Token(
                                                SyntaxKind.PrivateKeyword),
                                            Syntax.Token(
                                                SyntaxKind.StaticKeyword));
        }

        private static SeparatedSyntaxList<ParameterSyntax> GetParameterList(IEnumerable<IdentifierNameAndType> paramList)
        {
            var argumentSyntax = new List<SyntaxNodeOrToken>();
            foreach (var param in paramList)
            {
                argumentSyntax.Add(Syntax.Parameter(
                                        Syntax.Identifier(
                                            param.Name))
                                    .WithType(Syntax.ParseTypeName(param.Type)));

                if (param != paramList.Last())
                {
                    argumentSyntax.Add(Syntax.Token(SyntaxKind.CommaToken));
                }
            }

            return Syntax.SeparatedList<ParameterSyntax>(argumentSyntax.ToArray());
        }

        private static SeparatedSyntaxList<ArgumentSyntax> GetArgumentList(IEnumerable<IdentifierNameAndType> args)
        {
            var argumentSyntax = new List<SyntaxNodeOrToken>();
            foreach (var param in args)
            {
                argumentSyntax.Add(Syntax.Argument(Syntax.IdentifierName(param.Name)));

                if (param != args.Last())
                {
                    argumentSyntax.Add(Syntax.Token(SyntaxKind.CommaToken));
                }
            }

            return Syntax.SeparatedList<ArgumentSyntax>(argumentSyntax.ToArray());
        }

        private static ClassDeclarationSyntax GetClassDeclarationFromDocument(SyntaxNode namespaceDeclaration)
        {
            ClassDeclarationSyntax classDeclaration;
            classDeclaration = namespaceDeclaration.ChildNodes().OfType<ClassDeclarationSyntax>().FirstOrDefault();
            return classDeclaration;
        }


        private MethodDeclarationSyntax DeclareMethodWithType(string name, SemanticModel model)
        {
            var methoddecl = Syntax.ParseArgumentList(PredicateLocator.GetTypeStringForNode(semanticNodes.First(), model));

            return Syntax.MethodDeclaration(
                                        Syntax.ParseTypeName(PredicateLocator.GetTypeStringForNode(semanticNodes.First(), model)),
                                        Syntax.Identifier(name));
        }
    }
}