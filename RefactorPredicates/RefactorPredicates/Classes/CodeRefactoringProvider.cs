﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading;
using Roslyn.Compilers;
using Roslyn.Compilers.Common;
using Roslyn.Compilers.CSharp;
using Roslyn.Services;
using Roslyn.Services.Editor;
using System.Windows;

namespace RefactorPredicates
{
    [ExportCodeRefactoringProvider("RefactorPredicates", LanguageNames.CSharp)]
    public class CodeRefactoringProvider : ICodeRefactoringProvider
    {
        public CodeRefactoring GetRefactoring(IDocument document, TextSpan textSpan, CancellationToken cancellationToken)
        {
            var root = (SyntaxNode)document.GetSyntaxRoot(cancellationToken);

            var model = (SemanticModel)document.GetSemanticModel(cancellationToken);

            var lambdaFunctionList = PredicateLocator.FindExpressions(model);

            foreach (var expr in lambdaFunctionList)
            {
                //Check that it's in span and check duplicates last to benefit from short circuit evaluation
                if (expr.Span.Start <= textSpan.End 
                    && expr.Span.End >= textSpan.End
                    && lambdaFunctionList.Count(ex => PredicateLocator.LambdaLogicIsEqual(ex, expr, model) 
                        && PredicateLocator.LambdasAreSameType(model, expr, ex)) > 1
                    && !expr.DescendantTokens().Any(x => x.Kind == SyntaxKind.ThisKeyword || x.Kind == SyntaxKind.NewKeyword))
                {
                    return new CodeRefactoring(
                        new[] { new CodeAction(document, lambdaFunctionList.Where(x => PredicateLocator.LambdasAreSameType(model, expr, x) && PredicateLocator.LambdaLogicIsEqual(x, expr, model)).ToList()) },
                        expr.Span);
                }
            }

            return null;
        }
    }
}
