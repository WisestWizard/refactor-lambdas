﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefactorPredicates
{
    public class IdentifierNameAndType
    {
        public IdentifierNameAndType()
        {
            name = string.Empty;
            type = string.Empty;
        }

        private string name;
        private string type;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value.Trim();
            }
        }

        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value.Trim();
            }
        }
    }
}
