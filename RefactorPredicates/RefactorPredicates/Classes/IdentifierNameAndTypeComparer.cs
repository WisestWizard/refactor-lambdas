﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RefactorPredicates
{
    public class IdentifierNameAndTypeComparer : IEqualityComparer<IdentifierNameAndType>
    {
        public bool Equals(IdentifierNameAndType x, IdentifierNameAndType y)
        {
            if (x.Name == y.Name && x.Type == y.Type)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetHashCode(IdentifierNameAndType obj)
        {
            return obj.Name.GetHashCode() ^ obj.Type.GetHashCode();
        }
    }
}
