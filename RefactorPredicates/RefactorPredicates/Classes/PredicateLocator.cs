﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Roslyn.Compilers.Common;
using Roslyn.Compilers.CSharp;

namespace RefactorPredicates
{
    public static class PredicateLocator
    {
        public static IEnumerable<SimpleLambdaExpressionSyntax> FindExpressions(SemanticModel model)
        {
            var lambdaFunctionList = model.SyntaxTree.GetRoot().DescendantNodes()
                .OfType<ClassDeclarationSyntax>().First();

            if (lambdaFunctionList != null)
            {
                return lambdaFunctionList.DescendantNodes().Where(exp =>
                {
                    if (exp is SimpleLambdaExpressionSyntax)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }).Cast<SimpleLambdaExpressionSyntax>();
            }
            else
            {
                return new List<SimpleLambdaExpressionSyntax>();
            }
        }

        public static bool LambdaLogicIsEqual(SimpleLambdaExpressionSyntax source, SimpleLambdaExpressionSyntax target, SemanticModel model)
        {
            var sourceNodes = source.DescendantNodes().ToList();
            var targetNodes = target.DescendantNodes().ToList();

            if (sourceNodes.Count() != targetNodes.Count())
            {
                return false;
            }

            //Also check that they scope in the same # of parameters
            if (ExtractFunctionParameters(model, source).Count() != ExtractFunctionParameters(model, target).Count())
            {
                return false;
            }

            for (int i = 0; i < sourceNodes.Count(); i++)
            {
                if (sourceNodes[i] is BinaryExpressionSyntax && targetNodes[i] is BinaryExpressionSyntax)
                {
                    sourceNodes[i] = sourceNodes[i] as BinaryExpressionSyntax;
                    targetNodes[i] = targetNodes[i] as BinaryExpressionSyntax;
                    if (sourceNodes[i].Kind != targetNodes[i].Kind)
                    {
                        return false;
                    }
                }
                else if (sourceNodes[i] is IdentifierNameSyntax && targetNodes[i] is IdentifierNameSyntax)
                {
                    {
                        sourceNodes[i] = sourceNodes[i] as IdentifierNameSyntax;
                        targetNodes[i] = targetNodes[i] as IdentifierNameSyntax;
                        var typeInfoSource = model.GetSpeculativeTypeInfo(sourceNodes[i].Span.Start, Syntax.ParseExpression(sourceNodes[i].ToFullString()), SpeculativeBindingOption.BindAsExpression).Type;
                        var typeInfoTarget = model.GetSpeculativeTypeInfo(targetNodes[i].Span.Start, Syntax.ParseExpression(targetNodes[i].ToFullString()), SpeculativeBindingOption.BindAsExpression).Type;

                        if (typeInfoSource != typeInfoTarget)
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    if (sourceNodes[i].ToFullString() != targetNodes[i].ToFullString())
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public static string LambdaName(SimpleLambdaExpressionSyntax expression, SemanticModel model, ClassDeclarationSyntax classDeclaration)
        {
            var name = new StringBuilder();

            //Parameter information
            var param = expression.DescendantTokens().First(x => x.Kind == SyntaxKind.IdentifierToken);

            foreach (var token in expression.DescendantTokens())
            {
                //Make sure its not the param token and that it's an identifier
                if (token.Kind == SyntaxKind.IdentifierToken && token != expression.DescendantTokens().First())
                {
                    if (param.ToFullString() == token.ToFullString())
                    {
                        var typeInfo = model.GetSpeculativeTypeInfo(param.Span.Start, Syntax.ParseExpression(param.ToFullString()), SpeculativeBindingOption.BindAsExpression).Type;
                        name.Append(typeInfo.ToMinimalDisplayString(param.GetLocation(), model).Replace("System.", ""));
                    }
                    else
                    {
                        name.Append(token.Value);
                    }
                }

                if (token.Kind == SyntaxKind.EqualsEqualsToken)
                {
                    name.Append("Equals");
                }

                if (token.Kind == SyntaxKind.ExclamationEqualsToken)
                {
                    name.Append("NotEquals");
                }

                if (token.Kind == SyntaxKind.AmpersandAmpersandToken)
                {
                    name.Append("And");
                }

                if (token.Kind == SyntaxKind.BarBarToken)
                {
                    name.Append("Or");
                }

                if (token.Kind == SyntaxKind.NullKeyword)
                {
                    name.Append("Null");
                }

                if (token.Kind == SyntaxKind.TrueKeyword)
                {
                    name.Append("True");
                }

                if (token.Kind == SyntaxKind.FalseKeyword)
                {
                    name.Append("False");
                }

                if (token.Kind == SyntaxKind.NumericLiteralToken)
                {
                    name.Append(token.Value);
                }

            }

            var potentialName = string.Empty;
            int nextFun = 0;
            var namePostfix = string.Empty;
            do
            {
                if (name.Length > 70)
                {
                    potentialName = "RefactoredLambda" + namePostfix;
                }
                else
                {
                    potentialName = char.ToUpper(name[0]) + name.ToString().Substring(1) + namePostfix;
                }

                nextFun++;
                namePostfix = nextFun.ToString();
            } while (classDeclaration.DescendantNodes().OfType<MethodDeclarationSyntax>().Any(x => x.Identifier.ToFullString() == potentialName));

            return potentialName;
        }

        public static bool IsNotStaticOrNamed(CancellationToken cancellationToken, ISemanticModel Model, CommonSyntaxNode syntax)
        {
            //Don't want static functions in here
            return Model.GetSymbolInfo(syntax).Symbol != null
                && !Model.GetSymbolInfo(syntax).Symbol.IsStatic
                && Model.GetSymbolInfo(syntax).Symbol.Kind != CommonSymbolKind.NamedType;
        }

        public static bool LambdasAreSameType(SemanticModel model, SimpleLambdaExpressionSyntax source, SimpleLambdaExpressionSyntax target)
        {
            return PredicateLocator.GetTypeStringForNode(source, model) == PredicateLocator.GetTypeStringForNode(target, model);
        }

        public static string GetTypeStringForNode(SimpleLambdaExpressionSyntax node, SemanticModel model)
        {
            var realType = model.GetTypeInfo(node).ConvertedType;
            return realType.ToMinimalDisplayString(node.GetLocation(), model);
        }

        public static IEnumerable<IdentifierNameAndType> ExtractFunctionParameters(SemanticModel model, SimpleLambdaExpressionSyntax node)
        {
            //Check that it's not the main node's variable
            return FunctionHeaderParameters(model, node)
                .Distinct(new IdentifierNameAndTypeComparer())
                .Where(param => param.Name != node.Parameter.Identifier.ToString());
        }

        public static List<IdentifierNameAndType> FunctionHeaderParameters(SemanticModel model, SimpleLambdaExpressionSyntax node)
        {
            var outerVariableList = new List<IdentifierNameAndType>();

            var outerVariables = node.DescendantNodes()
                .Where(x => PredicateLocator.IsNotStaticOrNamed(CancellationToken.None, model, x)
                    && ((x is IdentifierNameSyntax && !(x.Parent is MemberAccessExpressionSyntax))
                    || (x is MemberAccessExpressionSyntax && !x.DescendantNodes().Any(n => n is MemberAccessExpressionSyntax))));

            foreach (var outerVar in outerVariables)
            {
                var typeInfo = model.GetSpeculativeTypeInfo(node.Span.Start, Syntax.ParseExpression(outerVar.ToFullString()), SpeculativeBindingOption.BindAsExpression).Type;

                if (typeInfo != null && !string.IsNullOrEmpty(typeInfo.Name))
                {
                    if (outerVar is MemberAccessExpressionSyntax)
                    {
                        var objNode = outerVar.DescendantNodes().First().ToFullString();
                        var typeInfoObj = model.GetSpeculativeTypeInfo(node.Span.Start, Syntax.ParseExpression(objNode), SpeculativeBindingOption.BindAsExpression).Type;

                        outerVariableList.Add(new IdentifierNameAndType
                        {
                            Name = objNode,
                            Type = typeInfoObj.ToMinimalDisplayString(outerVar.GetLocation(), model)
                        });
                    }
                    else if (outerVar is IdentifierNameSyntax)
                    {
                        outerVariableList.Add(new IdentifierNameAndType
                        {
                            Name = outerVar.ToFullString(),
                            Type = typeInfo.ToMinimalDisplayString(outerVar.GetLocation(), model)
                        });
                    }
                }

            }
            return outerVariableList;
        }
    }
}
